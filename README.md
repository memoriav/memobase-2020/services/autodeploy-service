# Autodeploy Service

Deploys Gitlab projects on Kubernetes

## Running the server

### Configuration

The server expects the following environment variables to be set:

* `GITLAB_REGISTRY`: URL to the gitlab registry where the deployable Docker images / Chart registries are stored
* `SECURE_TOKEN`: This token is sent by the gitlab webhook push notification as a header. 
  It serves to authenticate the webhook request.

### Production mode

It is assumed that the server is run inside a Docker container and deployed to
a Kubernetes cluster. Use the Dockerfile to build the image or directly use a
[pre-built
image](https://gitlab.switch.ch/memoriav/memobase-2020/services/autodeploy-service/container_registry/129).

### Development mode

1. It is recommended to use a
   [virtualenv](https://virtualenv.pypa.io/en/latest/). Inside the project
   root folder type:
  a. `virtualenv venv`
  b. `source venv/bin/activate`
  c. `pip install -r requirements.txt`
2. `FLASK_APP=autodeploy_service_app/wsgi.py flask run`

The development server is started on `0.0.0.0:5000` per default. To override
these settings, see `flask --help`.

## Deployment Workflow for Services

1. Gitlab notifies the service via a webhook that a new Docker image has been
   successfully created
2. The service installs the charts of the project. It follows different approaches for each environment.
   - For the `test` environment, the service installs the chart with the `develop` tag.
   - For the `stage` environment, the service installs the chart with the `latest` tag.
   - For the `prod` environment, the service installs the chart with a semver tag.

## Requirements and Conventions Regarding Services

The service makes certain assumptions about the project being deployed:

- The project is a Gitlab project.
- The project has a `main` and a `develop` branch.
- The project has a folder `helm-charts` in the root directory.
- A GitLab Webhook is set up to notify the service about new Docker images. The
  webhook must be configured to send a `push` event on `pipeline events` and use the endpoint of this service.
