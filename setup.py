from setuptools import setup

setup(
    name="autodeploy_service_app",
    packages=["autodeploy_service_app"],
    include_package_data=True,
    install_requires=["flask", "gunicorn", "flasgger"],
)
