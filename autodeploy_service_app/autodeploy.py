import json
import subprocess
from os import environ, listdir, path, getenv
from re import match
from shutil import rmtree
from tempfile import mkdtemp

from flask import request as flask_request
from flask.views import MethodView

from autodeploy_service_app.app import app


class AutoDeploy(MethodView):
    def post(self):
        """
        Deploy on k8s
        ---
        tags:
          - AutoDeploy
        requestBody:
            required: true
        responses:
          200:
            description: Success, job report gets returned
            schema:
              properties:
                status:
                  type: string
                  example: success/failure/ignore
                  enum: ['success', 'failure', 'ignore']
                report:
                  type: string
                  example: 'institutionXYZ-235323B: something failed!\n
                    institutionXYZ-235323C: that one went totally wrong'
          500:
            description: Impossible to get results
            schema:
              properties:
                error:
                  type: string
                  example: Unexpected Kafka error

        """

        status = "ignore"
        body = ""
        output = []

        try:
            headers = flask_request.headers
            secure_token = getenv("SECURE_TOKEN")
            if secure_token and not headers.get("X-Gitlab-Token"):
                msg = "Request does not have an X-Gitlab-Token in headers"
                app.logger.info(msg)
                return msg, 403
            if (
                secure_token
                and headers.get("X-Gitlab-Token").rstrip() != secure_token.rstrip()
            ):
                msg = "Request does not have a valid X-Gitlab-Token in headers"
                app.logger.warning(msg)
                return msg, 403
            body = json.loads(flask_request.data.decode("utf-8"))
            tag = ""
            branch = ""
            if body["object_attributes"]["tag"]:
                tag = body["object_attributes"]["ref"]
            branch = body["object_attributes"]["ref"]
            if body["object_attributes"]["status"] == "success":
                projectName = body["project"]["path_with_namespace"].split(
                    "/")[-1]
                repositoryUrl = body["project"]["git_http_url"]
                repositoryPath = body["project"]["path_with_namespace"]
                app.logger.info(
                    "deploy-request received from " + repositoryUrl)

                # if tag = semver: deploy on prod+stage
                if match(r"^(([0-9]+)\.([0-9]+)\.([0-9]+))$", tag):
                    app.logger.info(
                        "commit with semver-tag detected: installing on prod+stage"
                    )
                    pullChartUri = (
                        environ["GITLAB_REGISTRY"]
                        + "/"
                        + repositoryPath
                        + ":"
                        + tag
                        + "-chart"
                    )
                    msgs, status = installFromRepo(pullChartUri)
                    output.extend(msgs)
                # deploy on stage only
                elif branch == "master" or branch == "main":
                    app.logger.info(
                        "commit on branch master or main without semver-tag detected: "
                        + "installing on stage"
                    )
                    msgs, status = installFromDir(
                        repositoryUrl, projectName, "stage")
                    output.extend(msgs)
                # deploy on test
                elif branch == "develop":
                    app.logger.info(
                        "commit on branch develop without semver-tag detected: "
                        + "installing on test"
                    )
                    msgs, status = installFromDir(
                        repositoryUrl, projectName, "test")
                    output.extend(msgs)
                else:
                    app.logger.info(
                        "Commit is not on branch master or main and is not tagged, "
                        + "thus ignoring commit"
                    )
            else:
                status = "ignore"

        except subprocess.CalledProcessError as ex:
            msg = "command {} failed with return code {} -- STDOUT: {} -- STDERR: {}".format(
                ex.cmd, ex.returncode, ex.stdout, ex.stderr
            )
            output.append(msg)
            status = "failure"
            app.logger.warning(msg)
        except json.JSONDecodeError as ex:
            msg = "the json in the body could not be decoded: {}".format(
                str(ex))
            output.append(msg)
            status = "failure"
            app.logger.warning(msg)
        except Exception as ex:
            msg = "an exception occured: {}".format(str(ex))
            output.append(msg)
            status = "failure"
            app.logger.warning(msg)
        returnVal = {"status": status, "log": output, "body": body}
        if status == "success":
            app.logger.info(json.dumps(returnVal))
            return returnVal, 200
        elif status == "ignore":
            app.logger.debug(json.dumps(returnVal))
            return returnVal, 202
        else:
            app.logger.info(json.dumps(returnVal))
            return returnVal, 500


def _upgrade_installation(
    chartsDir, projectName, filenameBase, filename=None, env=None
):
    app.logger.debug(
        "upgrading new deployment for {}{}".format(
            projectName, "" if filename is None else ": " + filename
        )
    )
    try:
        cmd = "helm upgrade -i {}-deployment {}{}{}".format(
            filenameBase,
            "--set tag=develop " if env and env == "test" else "",
            ""
            if filename is None
            else "-f {} ".format(path.join(chartsDir, "helm-values", filename)),
            chartsDir,
        )
        app.logger.info(cmd)
        proc = subprocess.run(
            cmd, shell=True, capture_output=True, text=True, check=True
        )
        app.logger.info(
            "successfully installed helm chart{}".format(
                "" if filename is None else " with " + filename
            )
        )
        return (
            "installing {}-deployment\n{}\n{}".format(
                filenameBase, proc.stdout, proc.stderr
            ),
            "success",
        )
    except subprocess.CalledProcessError as ex:
        msg = "upgrading helm chart failed with return code {} -- STDOUT: {} -- STDERR: {}".format(
            ex.returncode, ex.stdout, ex.stderr
        )
        app.logger.warning(msg)
        return msg, "failure"


def installFromRepo(pullChartUri):
    output = []
    status = ""
    app.logger.debug("pulling helm charts")
    try:
        proc = subprocess.run(
            "helm chart pull " + pullChartUri,
            shell=True,
            capture_output=True,
            text=True,
            check=True,
        )
        output.append(
            "pulling charts from {}: {} (stderr: {})".format(
                pullChartUri, proc.stdout, proc.stderr
            )
        )
    except subprocess.CalledProcessError as ex:
        msg = "pulling helm chart failed with return code {} -- STDOUT: {} -- STDERR: {}".format(
            ex.returncode, ex.stdout, ex.stderr
        )
        app.logger.warning(msg)
        return output, "failure"
    pulledChartsDir = mkdtemp()
    app.logger.debug("exporting helm charts")
    try:
        proc = subprocess.run(
            "helm chart export " + pullChartUri + " -d " + pulledChartsDir,
            shell=True,
            capture_output=True,
            text=True,
            check=True,
        )
        output.append(
            "exporting charts to local directory: {} (stderr: {})".format(
                proc.stdout, proc.stderr
            )
        )
    except subprocess.CalledProcessError as ex:
        msg = "exporting helm chart failed with return code {} -- STDOUT: {} -- STDERR: {}".format(
            ex.returncode, ex.stdout, ex.stderr
        )
        app.logger.warning(msg)
        return output, "failure"
    projectName = listdir(pulledChartsDir)[0]
    helmChartDir = path.join(pulledChartsDir, projectName)
    helmValuesDir = path.join(helmChartDir, "helm-values")
    if path.exists(helmValuesDir):
        app.logger.debug("helm value files detected")
        valuesFound = False
        for filename in listdir(helmValuesDir):
            filenameBase = path.splitext(filename)[0]
            if "-prod." in filename and status != "failure":
                app.logger.info(
                    "upgrading helm chart {} for prod environment".format(
                        projectName)
                )
                msg, status = _upgrade_installation(
                    helmChartDir, projectName, filenameBase, filename
                )
                output.append(msg)
                valuesFound = True
            elif "-stage." in filename and status != "failure":
                app.logger.info(
                    "upgrading helm chart {} for stage environment".format(
                        projectName)
                )
                msg, status = _upgrade_installation(
                    helmChartDir, projectName, filenameBase, filename
                )
                output.append(msg)
                valuesFound = True
        if not valuesFound:
            msg = "no helm chart for prod or stage environment found"
            app.logger.info(msg)
            output.append(msg)
            status = "ignore"
    else:
        msg = "Could not find path to exported helm chart"
        app.logger.warn(msg)
        output.append(msg)
        status = "failure"
    rmtree(pulledChartsDir)
    try:
        proc = subprocess.run(
            "helm chart remove {}".format(pullChartUri),
            shell=True,
            capture_output=True,
            text=True,
            check=True,
        )
    except subprocess.CalledProcessError as ex:
        msg = (
            "removing helm chart from local repository"
            + "failed with return code {} -- STDOUT: {} -- STDERR: {}".format(
                ex.returncode, ex.stdout, ex.stderr
            )
        )
        app.logger.warning(msg)
    return output, status


def installFromDir(repositoryUrl, projectName, environment):
    output = []
    status = ""
    app.logger.debug("cloning repository")
    repoDir = mkdtemp()
    try:
        subprocess.run(
            "git clone {} {}".format(repositoryUrl, repoDir),
            shell=True,
            capture_output=True,
            text=True,
            check=True,
        )
        output.append("git clone {}".format(repositoryUrl))
        if environment == "test":
            subprocess.run(
                "git checkout develop",
                shell=True,
                capture_output=True,
                text=True,
                check=True,
                cwd=repoDir,
            )
            output.append("git checkout develop")
    except subprocess.CalledProcessError as ex:
        msg = "cloning git repo failed with return code {} -- STDOUT: {} -- STDERR: {}".format(
            ex.returncode, ex.stdout, ex.stderr
        )
        app.logger.warning(msg)
        return output, "failure"

    helmChartDir = path.join(repoDir, "helm-charts")
    if not path.exists(helmChartDir):
        msg = "No helm-charts directory exists"
        app.logger.warn(msg)
        output.append(msg)
        return output, "failure"
    helmValueDir = path.join(helmChartDir, "helm-values")
    if path.exists(helmValueDir):
        app.logger.debug("helm value files detected")
        for filename in listdir(helmValueDir):
            filenameBase = path.splitext(filename)[0]
            if environment == "test" and "-test." in filename:
                app.logger.info(
                    "upgrading helm chart {} for test environment".format(
                        projectName)
                )
                msg, status = _upgrade_installation(
                    helmChartDir, projectName, filenameBase, filename, "test"
                )
                output.append(msg)
            if environment == "stage" and "-stage." in filename:
                app.logger.info(
                    "upgrading helm chart {} for stage environment".format(
                        projectName)
                )
                msg, status = _upgrade_installation(
                    helmChartDir, projectName, filenameBase, filename
                )
                output.append(msg)
    else:
        # this section should be obsolete since we shouldn't have helm-charts w/o valuefiles
        app.logger.warn("no helm value files detected. this is DEPRECATED")
        msg, status = _upgrade_installation(
            helmChartDir, projectName, projectName)
        output.append(msg)
    app.logger.debug("Removing temp dir")
    rmtree(repoDir)
    return output, status
