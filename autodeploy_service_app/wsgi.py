import logging
from os import path, environ

from flasgger import Swagger
from flask import send_from_directory, redirect

from autodeploy_service_app.app import app
from autodeploy_service_app.autodeploy import AutoDeploy

environ["HELM_EXPERIMENTAL_OCI"] = "1"

# If app is started via gunicorn
if __name__ != "__main__":
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger = gunicorn_logger
    app.logger.info("Starting production server")

app.config["SWAGGER"] = {
    "title": "autodeploy-service",
    "version": "dev",
    "uiversion": 3,
    "termsOfService": "http://memobase.ch/de/disclaimer",
    "description": "service to deploy on k8s form a gitlab webhook",
    "contact": {
        "name": "UB Basel",
        "url": "https://ub.unibas.ch",
        "email": "swissbib-ub@unibas.ch",
    },
    "favicon": "/favicon.ico",
}
Swagger(app)


@app.route("/")
def home():
    return redirect("/apidocs")


@app.route("/favicon.ico")
def favicon():
    return send_from_directory(
        path.join(app.root_path, "assets"),
        "favicon.ico",
        mimetype="image/vnd.microsoft.icon",
    )


app.add_url_rule("/v1/autodeploy", view_func=AutoDeploy.as_view("autodeploy"))

# If app is started with Flask
if __name__ == "__main__":
    logging.basicConfig(format="%(levelname)-8s [%(filename)s:%(lineno)d] %(message)s")
    app.logger.info("Starting development server")
    app.run(host="0.0.0.0", port=5000, debug=True)
